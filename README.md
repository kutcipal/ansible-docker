# README #

Ansible playbook to create a EC2 instance and configure it to run a bitcoin daemon inside a container

### What is this repository for? ###

* Interview question

### How do I get set up? ###

* Clone the repo
* Dependencies - Ansible and AWS account
* Deployment instructions
`ansible-playbook -v build_docker.yml --extra-vars "region=<AWS REGION> env=<ENVIRONMENT> type=docker"`

**EXAMPLE**  
`ansible-playbook -v build_docker.yml --extra-vars "region=us-east-1 env=test type=docker"`